# certificates
# Web-программирование 2
# Сертификат Udemy
# Build eCommerce websites with WordPress & WooCommerce
https://www.udemy.com/certificate/UC-a08b498e-5c4c-4c0d-9e44-19537c57d856/?utm_medium=email&utm_campaign=email&utm_source=sendgrid.com
![Снимок](https://gitlab.com/isthatzarema/certificates/-/raw/main/udemy.jpg)
# Сертификаты Intuit
# Введение в Django
https://intuit.ru/verifydiplomas/101490856

![Снимок](https://gitlab.com/isthatzarema/certificates/-/raw/main/django.jpg)
# Web-программирование на PHP 5.2
https://intuit.ru/verifydiplomas/101490868

![Снимок](https://gitlab.com/isthatzarema/certificates/-/raw/main/php.jpg)
# Операционная система Linux
https://intuit.ru/verifydiplomas/101490875

![Снимок](https://gitlab.com/isthatzarema/certificates/-/raw/main/linux.jpg)
